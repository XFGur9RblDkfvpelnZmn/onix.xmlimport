﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace ONIX.XMLImport
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string sConnectionString = Properties.Settings.Default.ConnectionString;

            if (!args.Contains("EXPORT"))
            {
                foreach (string sPath in Directory.GetFiles(Properties.Settings.Default.OnixPath, "*.onx"))
                {
                    try
                    {
                        using (SqlConnection con = new SqlConnection(sConnectionString))
                        {

                            using (SqlCommand cmd = new SqlCommand(Properties.Settings.Default.StoredProcedureImportName, con))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                string sInhoud = System.IO.File.ReadAllText(sPath).Replace(" encoding=\"UTF-8\"", "");
                                byte[] bytes = Encoding.Default.GetBytes(sInhoud);
                                string s = Encoding.UTF8.GetString(bytes);
                                cmd.Parameters.Add("@XML", SqlDbType.Text).Value = s;


                                con.Open();
                                cmd.ExecuteNonQuery();
                            }

                        }
                        FileInfo fi = new FileInfo(sPath);
                        Random rnd = new Random();
                        string archiveFileName = Properties.Settings.Default.ArchivePath + "\\" + fi.Name + "_" + rnd.Next(0, 999999);
                        fi.MoveTo(archiveFileName);

                        using (FileStream fs = new FileStream(Properties.Settings.Default.LogPath+ "\\Log.txt", FileMode.Append, FileAccess.Write))
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.WriteLine("----------------------------------------------");
                            sw.WriteLine("Type: SUCCESFULL IMPORT");
                            sw.WriteLine("Date: " + DateTime.Now.ToLongDateString());
                            sw.WriteLine("Time: " + DateTime.Now.ToLongTimeString());
                            sw.WriteLine("File: " + sPath);
                            sw.WriteLine("----------------------------------------------");

                        }
                    }
                    catch(Exception ex)
                    {
                        FileInfo fi = new FileInfo(sPath);
                        Random rnd = new Random();
                        string errorFileName = Properties.Settings.Default.ErrorPath+"\\"+fi.Name+"_"+rnd.Next(0,999999);
                        File.Move(fi.FullName,errorFileName);
                        using (FileStream fs = new FileStream(Properties.Settings.Default.ErrorPath+"\\Log.txt",FileMode.Append, FileAccess.Write))
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.WriteLine("----------------------------------------------");
                            sw.WriteLine("Type: IMPORT");
                            sw.WriteLine("File: " + errorFileName);
                            sw.WriteLine("Date: " + DateTime.Now.ToLongDateString());
                            sw.WriteLine("Time: " + DateTime.Now.ToLongTimeString());
                            sw.WriteLine("----------------------------------------------");
                            sw.WriteLine("Error:");
                            sw.WriteLine(ex.ToString());
                        }


                    }
                    
                }
            }
            else
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(sConnectionString))
                    {

                        using (SqlCommand cmd = new SqlCommand(Properties.Settings.Default.StoredProcedureExportName, con))
                        {
                            DataTable allData = new DataTable();

                            cmd.CommandType = CommandType.StoredProcedure;
                            con.Open();

                            //SqlDataReader reader = cmd.ExecuteReader();
                            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                            adapter.Fill(allData);
                            String fileName = Properties.Settings.Default.ExportPath + "\\onix_" + DateTime.Now.ToString("yyyyMMdd_HHmm_fff")+".csv";
                            using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                            using (StreamWriter sw = new StreamWriter(fs))
                            {
                                foreach (DataRow row in allData.Rows)
                                {
                                    foreach (object item in row.ItemArray)
                                    {
                                        sw.Write(item.ToString() + Properties.Settings.Default.ExportDelimiter);
                                    }
                                    sw.WriteLine();
                                }
                            }
                            //cmd.ExecuteNonQuery();
                        }

                    }
                    using (FileStream fs = new FileStream(Properties.Settings.Default.LogPath+ "\\Log.txt", FileMode.Append, FileAccess.Write))
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine("----------------------------------------------");
                        sw.WriteLine("Type: SUCCESFULL EXPORT");
                        sw.WriteLine("Date: " + DateTime.Now.ToLongDateString());
                        sw.WriteLine("Time: " + DateTime.Now.ToLongTimeString());
                        sw.WriteLine("----------------------------------------------");
                        
                    }
                }
                catch (Exception ex)
                {
                    
                    using (FileStream fs = new FileStream(Properties.Settings.Default.LogPath+ "\\Log.txt", FileMode.Append, FileAccess.Write))
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine("----------------------------------------------");
                        sw.WriteLine("Type: EXPORT");
                        sw.WriteLine("Date: " + DateTime.Now.ToLongDateString());
                        sw.WriteLine("Time: " + DateTime.Now.ToLongTimeString());
                        sw.WriteLine("----------------------------------------------");
                        sw.WriteLine("Error:");
                        sw.WriteLine(ex.ToString());
                    }


                }
            }
            
        }
    }
}
