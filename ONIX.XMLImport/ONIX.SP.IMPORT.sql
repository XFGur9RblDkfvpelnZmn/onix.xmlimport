USE [ONIX]
GO

/****** Object:  StoredProcedure [dbo].[IMPORT]    Script Date: 11/11/2014 20:29:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IMPORT]
	@XML xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	WITH XMLNAMESPACES (        
	   DEFAULT 'http://ns.editeur.org/onix/3.0/reference'
		)
    SELECT
	 T.N.value('ProductIdentifier[1]/IDValue[1]','Varchar (19)') AS PR_ITEMCODE
			,T.N.value('ProductIdentifier[1]/IDValue[1]','Varchar (19)') AS PR_SUPPLIERCODE
			,T.N.value('DescriptiveDetail[1]/TitleDetail[1]/TitleElement[1]/TitleText[1]','Varchar (35)') AS PR_DESCRIPTION
			,T.N.value('ProductSupply[1]/SupplyDetail[1]/Price[1]/Tax[1]/TaxRateCode[1]','Varchar (18)') AS PR_MAINGROUP
			,T.N.value('DescriptiveDetail[1]/Subject[1]/SubjectCode[1]','Varchar (18)') AS PR_SUBGROUP
			,T.N.value('ProductSupply[1]/SupplyDetail[1]/Price[1]/Tax[1]/TaxRateCode[1]','Varchar (1)') AS PR_TAX 
			,ISNULL(T.N.value('DescriptiveDetail[1]/Contributor[1]/KeyNames[1]','Varchar (35)') + ',' ,NULL) 
				
				+ ISNULL(T.N.value('DescriptiveDetail[1]/Contributor[1]/NamesBeforeKey[1]','Varchar (35)') ,'')
				+ ISNULL(T.N.value('DescriptiveDetail[1]/Contributor[1]/PrefixToKey[1]','Varchar (35)'),'') AS PR_DIV1 
			,T.N.value('DescriptiveDetail[1]/Subject[1]/SubjectCode[1]','Varchar (35)') AS PR_DIV2
			,T.N.value('PublishingDetail[1]/Imprint[1]/ImprintIdentifier[1]/IDValue[1]','Varchar (35)') AS PR_DIV4
			,T.N.value('PublishingDetail[1]/PublishingDate[1]/Date[1]','Varchar (35)') AS PR_DIV5
			,T.N.value('ProductSupply[1]/SupplyDetail[1]/Price[1]/PriceAmount[1]','Numeric (19,2)') AS PR_RETAILPRICE
			,T.N.value('DescriptiveDetail[1]/ProductForm[1]','Varchar (5)') AS PR_PRODUCTFORM	
			,T.N.value('ProductSupply[1]/SupplyDetail[1]/ProductAvailability[1]','Varchar (5)') AS PR_PRODUCTAVAILABILITY
		INTO #Temp
		from @XML.nodes('./ONIXMessage[@release=3.0]/Product') T(N);
	
	WITH TARGET AS (
		SELECT [PR_ITEMCODE]
		  ,[PR_SUPPLIERCODE]
		  ,[PR_DESCRIPTION]	
		  ,[PR_MAINGROUP]
		  ,[PR_SUBGROUP]
		  ,[PR_TAX]
		  ,[PR_DIV1]
		  ,[PR_DIV2]
		  ,[PR_DIV4]
		  ,[PR_DIV5]
		  ,[PR_RETAILPRICE]
		  ,[PR_PRODUCTFORM]
		  ,[PR_PRODUCTAVAILABILITY]
		FROM PRODUCTS
	)
	MERGE INTO TARGET
	USING #Temp AS SOURCE ON SOURCE.PR_ITEMCODE = TARGET.PR_ITEMCODE
	WHEN NOT MATCHED BY TARGET  
		THEN INSERT([PR_ITEMCODE]
		  ,[PR_SUPPLIERCODE]
		  ,[PR_DESCRIPTION]	
		  ,[PR_MAINGROUP]
		  ,[PR_SUBGROUP]
		  ,[PR_TAX]
		  ,[PR_DIV1]
		  ,[PR_DIV2]
		  ,[PR_DIV4]
		  ,[PR_DIV5]
		  ,[PR_RETAILPRICE]
		  ,[PR_PRODUCTFORM]
		  ,[PR_PRODUCTAVAILABILITY]) 
		VALUES
		  (SOURCE.PR_ITEMCODE
		  ,SOURCE.PR_SUPPLIERCODE
		  ,SOURCE.PR_DESCRIPTION	
		  ,SOURCE.PR_MAINGROUP
		  ,SOURCE.PR_SUBGROUP
		  ,SOURCE.PR_TAX
		  ,SOURCE.PR_DIV1
		  ,SOURCE.PR_DIV2
		  ,SOURCE.PR_DIV4
		  ,SOURCE.PR_DIV5
		  ,SOURCE.PR_RETAILPRICE
		  ,SOURCE.PR_PRODUCTFORM
		  ,SOURCE.PR_PRODUCTAVAILABILITY)
	WHEN MATCHED
		THEN UPDATE SET 
			TARGET.		  PR_ITEMCODE	 = SOURCE.	PR_ITEMCODE
			,TARGET.	  PR_SUPPLIERCODE	 = SOURCE.	  PR_SUPPLIERCODE
			,TARGET.	  PR_DESCRIPTION	 = SOURCE.	  PR_DESCRIPTION
			,TARGET.	  PR_MAINGROUP	 = SOURCE.	  PR_MAINGROUP
			,TARGET.	  PR_SUBGROUP	 = SOURCE.	  PR_SUBGROUP
			,TARGET.	  PR_TAX	 = SOURCE.	  PR_TAX
			,TARGET.	  PR_DIV1	 = SOURCE.	  PR_DIV1
			,TARGET.	  PR_DIV2	 = SOURCE.	  PR_DIV2
			,TARGET.	  PR_DIV4	 = SOURCE.	  PR_DIV4
			,TARGET.	  PR_DIV5	 = SOURCE.	  PR_DIV5
			,TARGET.	  PR_RETAILPRICE	 = SOURCE.	  PR_RETAILPRICE
			,TARGET.	  PR_PRODUCTFORM	 = SOURCE.	  PR_PRODUCTFORM
			,TARGET.	  PR_PRODUCTAVAILABILITY	 = SOURCE.	  PR_PRODUCTAVAILABILITY
		OUTPUT $action, inserted.*, deleted.*;
	return 1
END

GO


