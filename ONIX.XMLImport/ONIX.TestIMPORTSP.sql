USE [ONIX]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[IMPORT]
		@XML = N'<?xml version="1.0"?>
<ONIXMessage release="3.0" xmlns="http://ns.editeur.org/onix/3.0/reference">
  <Header>
    <Sender>
      <SenderIdentifier>
        <SenderIDType>10</SenderIDType>
        <IDValue>8894126</IDValue>
      </SenderIdentifier>
      <SenderName>CB B.V.</SenderName>
      <ContactName>Content</ContactName>
      <EmailAddress>content@cb-logistics.nl</EmailAddress>
    </Sender>
    <MessageNumber>16441538</MessageNumber>
    <SentDateTime>20141027T1144</SentDateTime>
  </Header>
<Product>
  <RecordReference>9789025861964</RecordReference>
  <NotificationType>04</NotificationType>
  <RecordSourceType>09</RecordSourceType>
  <ProductIdentifier>
    <ProductIDType>03</ProductIDType>
    <IDValue>9789025861964</IDValue>
  </ProductIdentifier>
  <DescriptiveDetail>
    <ProductComposition>00</ProductComposition>
    <ProductForm>BB</ProductForm>
    <Measure>
      <MeasureType>01</MeasureType>
      <Measurement>223</Measurement>
      <MeasureUnitCode>mm</MeasureUnitCode>
    </Measure>
    <Measure>
      <MeasureType>02</MeasureType>
      <Measurement>154</Measurement>
      <MeasureUnitCode>mm</MeasureUnitCode>
    </Measure>
    <Measure>
      <MeasureType>03</MeasureType>
      <Measurement>8</Measurement>
      <MeasureUnitCode>mm</MeasureUnitCode>
    </Measure>
    <Measure>
      <MeasureType>08</MeasureType>
      <Measurement>171</Measurement>
      <MeasureUnitCode>gr</MeasureUnitCode>
    </Measure>
    <ProductClassification>
      <ProductClassificationType>06</ProductClassificationType>
      <ProductClassificationCode>125</ProductClassificationCode>
    </ProductClassification>
    <ProductClassification>
      <ProductClassificationType>07</ProductClassificationType>
      <ProductClassificationCode>BK_KIND</ProductClassificationCode>
      <Percent>100</Percent>
    </ProductClassification>
    <TitleDetail>
      <TitleType>01</TitleType>
      <TitleElement>
        <TitleElementLevel>01</TitleElementLevel>
        <TitleText>Wonderpony</TitleText>
      </TitleElement>
    </TitleDetail>
    <TitleDetail>
      <TitleType>05</TitleType>
      <TitleElement>
        <TitleElementLevel>01</TitleElementLevel>
        <TitleText>WONDERPONY E4</TitleText>
      </TitleElement>
    </TitleDetail>
    <TitleDetail>
      <TitleType>10</TitleType>
      <TitleElement>
        <TitleElementLevel>01</TitleElementLevel>
        <TitleText>REIJNDERS*WONDERPONY E4</TitleText>
      </TitleElement>
    </TitleDetail>
    <TitleDetail>
      <TitleType>11</TitleType>
      <TitleElement>
        <TitleElementLevel>01</TitleElementLevel>
        <TitleText>Wonderpony E4</TitleText>
      </TitleElement>
    </TitleDetail>
    <Contributor>
      <SequenceNumber>1</SequenceNumber>
      <ContributorRole>A01</ContributorRole>
      <NamesBeforeKey>Joke</NamesBeforeKey>
      <KeyNames>Reijnders</KeyNames>
    </Contributor>
    <Contributor>
      <SequenceNumber>1</SequenceNumber>
      <ContributorRole>A01</ContributorRole>
      <NameType>00</NameType>
      <NamesBeforeKey>Joke</NamesBeforeKey>
      <KeyNames>Reijnders</KeyNames>
    </Contributor>
    <Contributor>
      <SequenceNumber>2</SequenceNumber>
      <ContributorRole>A12</ContributorRole>
      <NamesBeforeKey>Marieke</NamesBeforeKey>
      <KeyNames>Nelissen</KeyNames>
    </Contributor>
    <Contributor>
      <SequenceNumber>2</SequenceNumber>
      <ContributorRole>A12</ContributorRole>
      <NameType>02</NameType>
      <NamesBeforeKey>Marieke</NamesBeforeKey>
      <KeyNames>Nelissen</KeyNames>
    </Contributor>
    <Contributor>
      <SequenceNumber>2</SequenceNumber>
      <ContributorRole>A12</ContributorRole>
      <NameType>00</NameType>
      <NamesBeforeKey>Marieke</NamesBeforeKey>
      <KeyNames>Nelissen</KeyNames>
    </Contributor>
    <EditionNumber>1</EditionNumber>
    <Language>
      <LanguageRole>01</LanguageRole>
      <LanguageCode>dut</LanguageCode>
    </Language>
    <Extent>
      <ExtentType>00</ExtentType>
      <ExtentValue>28</ExtentValue>
      <ExtentUnit>03</ExtentUnit>
    </Extent>
    <Illustrated>02</Illustrated>
    <Subject>
      <MainSubject/>
      <SubjectSchemeIdentifier>32</SubjectSchemeIdentifier>
      <SubjectCode>287</SubjectCode>
    </Subject>
  </DescriptiveDetail>
  <CollateralDetail>
    <TextContent>
      <TextType>05</TextType>
      <ContentAudience>03</ContentAudience>
      <Text>Floors liefste wens komt uit. 
Ze krijgt een pony! 
Voor haar alleen. 
Maar Flonker valt een beetje tegen. 
Hij is te dik, niet mooi en niet lief. 

Maar dan ontdekt Floor iets. 
Wat is er met Flonker aan de hand? 

Zelf lezen E4</Text>
    </TextContent>
    <SupportingResource>
      <ResourceContentType>02</ResourceContentType>
      <ContentAudience>03</ContentAudience>
      <ResourceMode>03</ResourceMode>
      <ResourceVersion>
        <ResourceForm>02</ResourceForm>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>01</ResourceVersionFeatureType>
          <FeatureValue>D502</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>02</ResourceVersionFeatureType>
          <FeatureValue>800</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>03</ResourceVersionFeatureType>
          <FeatureValue>543</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>04</ResourceVersionFeatureType>
          <FeatureValue>9789025861964_bcovr.jpg</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>05</ResourceVersionFeatureType>
          <FeatureValue>.052</FeatureValue>
        </ResourceVersionFeature>
        <ResourceLink>https://cbonline.boekhuis.nl/pls/cover/p_get_cover_fe?p_hash=8C893096C126381BF96D81EBFDB723DF</ResourceLink>
        <ContentDate>
          <ContentDateRole>01</ContentDateRole>
          <Date dateformat="00">20130110</Date>
        </ContentDate>
      </ResourceVersion>
    </SupportingResource>
    <SupportingResource>
      <ResourceContentType>01</ResourceContentType>
      <ContentAudience>03</ContentAudience>
      <ResourceMode>03</ResourceMode>
      <ResourceVersion>
        <ResourceForm>02</ResourceForm>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>01</ResourceVersionFeatureType>
          <FeatureValue>D502</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>02</ResourceVersionFeatureType>
          <FeatureValue>800</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>03</ResourceVersionFeatureType>
          <FeatureValue>543</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>04</ResourceVersionFeatureType>
          <FeatureValue>9789025861964_covr.jpg</FeatureValue>
        </ResourceVersionFeature>
        <ResourceVersionFeature>
          <ResourceVersionFeatureType>05</ResourceVersionFeatureType>
          <FeatureValue>.0859</FeatureValue>
        </ResourceVersionFeature>
        <ResourceLink>https://cbonline.boekhuis.nl/pls/cover/p_get_cover_fe?p_hash=42AD6D313605852D27A6D1DF16A4AFD6</ResourceLink>
        <ContentDate>
          <ContentDateRole>01</ContentDateRole>
          <Date dateformat="00">20130304</Date>
        </ContentDate>
      </ResourceVersion>
    </SupportingResource>
  </CollateralDetail>
  <PublishingDetail>
    <Publisher>
      <PublishingRole>01</PublishingRole>
      <PublisherIdentifier>
        <PublisherIDType>10</PublisherIDType>
        <IDValue>7500190</IDValue>
      </PublisherIdentifier>
      <PublisherName>Leopold B.V.</PublisherName>
    </Publisher>
    <PublishingStatus>04</PublishingStatus>
    <PublishingDate>
      <PublishingDateRole>01</PublishingDateRole>
      <Date dateformat="00">20130514</Date>
    </PublishingDate>
  </PublishingDetail>
  <RelatedMaterial/>
  <ProductSupply>
    <SupplyDetail>
      <Supplier>
        <SupplierRole>00</SupplierRole>
        <SupplierIdentifier>
          <SupplierIDType>13</SupplierIDType>
          <IDValue>WPGLEO</IDValue>
        </SupplierIdentifier>
        <SupplierName>CB</SupplierName>
      </Supplier>
      <ProductAvailability>21</ProductAvailability>
      <SupplyDate>
        <SupplyDateRole>08</SupplyDateRole>
        <Date dateformat="00">20130514</Date>
      </SupplyDate>
      <PackQuantity>72</PackQuantity>
      <Price>
        <PriceType>04</PriceType>
        <PriceCondition>
          <PriceConditionType>00</PriceConditionType>
        </PriceCondition>
        <DiscountCoded>
          <DiscountCodeType>03</DiscountCodeType>
          <DiscountCode>A</DiscountCode>
        </DiscountCoded>
        <PriceAmount>6.95</PriceAmount>
        <Tax>
          <TaxRateCode>R</TaxRateCode>
          <TaxRatePercent>6.00</TaxRatePercent>
          <TaxableAmount>6.56</TaxableAmount>
        </Tax>
        <CurrencyCode>EUR</CurrencyCode>
        <PriceDate>
          <PriceDateRole>14</PriceDateRole>
          <Date dateformat="00">20130619</Date>
        </PriceDate>
      </Price>
      <Price>
        <PriceType>01</PriceType>
        <PriceCondition>
          <PriceConditionType>00</PriceConditionType>
        </PriceCondition>
        <DiscountCoded>
          <DiscountCodeType>03</DiscountCodeType>
          <DiscountCode>A</DiscountCode>
        </DiscountCoded>
        <PriceAmount>6.56</PriceAmount>
        <Tax>
          <TaxRateCode>R</TaxRateCode>
          <TaxRatePercent>6.00</TaxRatePercent>
          <TaxableAmount>6.56</TaxableAmount>
        </Tax>
        <CurrencyCode>EUR</CurrencyCode>
        <PriceDate>
          <PriceDateRole>14</PriceDateRole>
          <Date dateformat="00">20130619</Date>
        </PriceDate>
      </Price>
    </SupplyDetail>
  </ProductSupply>
</Product>
</ONIXMessage>'

SELECT	'Return Value' = @return_value

GO
