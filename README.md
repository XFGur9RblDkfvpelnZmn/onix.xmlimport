```
# README #
Use the ONIX.XMLImport.exe.config file to configure the following parameteres:   
* ConnectionString: self explaining  
* OnixPath: Path to the directory containing .onx files  
* StoredProcedureName: name of the stored procedure which handles the xml import, the stored procedure needs to accept a @ XML parameter  
  
Use ONIX.SP.IMPORT.sql to create the IMPORT stored procedure on the database, ONIX.TestIMPORTSP.sql can be used to test the IMPORT stored procedure manually in management studio  
  
TO DO:  
* Default wanneer velden niet aangeleverd worden is nu NULL, de waarde wordt dan ook geupdate, even overleggen of NULL waarden overgeslaan moeten worden  
 * ProductAvailability is niet aanwezig in PublishingDetail > PublishingDate voor PR_DVIV5, nu maar even enkel Date genomen  
 * ProductForm zit niet in ProductComposition maar erachter, dus neem aan dat dat ProductForm bedoeld wordt  
 * IDValue zit in ImprintIdentifier  
 * PR_DVIV1  is PR_DIV1 in DB  
 * exception handling   
 * C# tool to handle export to csv flies  
 * Check if update is necessary